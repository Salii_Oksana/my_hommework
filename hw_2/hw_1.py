# Як вхідні дані запитайте ціле число. Якщо воно ділиться на 3, виведіть "foo";
# якщо воно ділиться на 5, виведіть "bar";
# якщо воно ділиться на обидва, виведіть "ham" (а не "foo" або "bar").

user_input = int(input("Number is "))

is_divided_by_three = user_input % 3 == 0

is_divided_by_five = user_input % 7 == 0

is_divided_by_three_and_five = is_divided_by_three and is_divided_by_five

if is_divided_by_three_and_five:
    print("ham")
elif is_divided_by_three:
    print("foo")
elif is_divided_by_five:
    print("bar")
else:
    print("No condition is met")