# Зіграйте у гру Fizz - Buzz:виведіть усі числа від 1 до 100;
# якщо число ділиться на 3, замість числа виведіть ("fizz").
# Якщо воно ділиться на 5, замість числа виведіть("Buzz").
# Якщо воно ділиться на обидва, виведіть "fizz buzz" замість числа.

user_input = int(input("Number is "))

is_divided_by_three = user_input % 3 == 0

is_divided_by_five = user_input % 7 == 0

is_divided_by_three_and_five = user_input % 3 == 0 and  user_input % 7 == 0

if is_divided_by_three_and_five:
    print("Fizz-Buzz")
elif is_divided_by_three:
    print("Fizz")
elif is_divided_by_five:
    print("Buzz")
else:
    print(user_input)
