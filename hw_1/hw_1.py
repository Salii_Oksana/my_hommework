user_name = input("your name ")
titled_user_name = user_name.title()

monthly_salary_in_dollar = int(input("Your month salary is "))
annual_salary_in_dollar = monthly_salary_in_dollar * 12

if annual_salary_in_dollar < 1000:
    formated_annual_salary = 0
else:
    formated_annual_salary = str(annual_salary_in_dollar)[:-3]

result_text = f"Annual salary {titled_user_name} makes up {formated_annual_salary} thousands dollars"
print(result_text)
